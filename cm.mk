$(call inherit-product-if-exists, device/prestigio/pap4040duo/pap4040duo.mk)

# Common CM stuff
$(call inherit-product-if-exists, vendor/cm/config/common_full_phone.mk)

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_FINGERPRINT=6.0/MRA58M/2280749:user/release-keys PRIVATE_BUILD_DESC="pap4040duo-user 6.0 MRA58M 2280749 release-keys"

PRODUCT_NAME := cm_pap4040duo
PRODUCT_DEVICE := pap4040duo
PRODUCT_BRAND := prestigio
PRODUCT_MANUFACTURER := Prestigio
PRODUCT_MODEL := PAP4040DUO

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_DEVICE="pap4040duo"
